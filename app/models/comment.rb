class Comment
include Mongoid::Document
	field :c_content, type: String
	
	validates :c_content, presence: true
	validates :topic_id, presence: true
	
	belongs_to :topic
	belongs_to :user
end