class Topic
  include Mongoid::Document
  include Mongoid::Timestamps
  attr_accessor :choosen_option
  
  field :name, type: String
  validates :name, presence: true


  has_many :options
  belongs_to :user
  has_many :comments
  
  validates :user_id, presence: true

  paginates_per 6

end
