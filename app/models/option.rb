class Option
  include Mongoid::Document
  field :content, type: String
  field :vote, type: Integer, :default => 0
  validates :content, presence: true
  validates :vote, presence: true
  validates :topic_id, presence: true
  
  belongs_to :topic
end
