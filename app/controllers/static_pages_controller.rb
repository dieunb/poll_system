class StaticPagesController < ApplicationController
  def home
  		@topics = Topic.asc(:name).page params[:page]
  end

  def help
  end

  def about
  end

end
