class TopicsController < ApplicationController

  def embed
    @topic = Topic.find(params[:id])
    render action: "embed", :layout => "embed.html" 
  end

  def embed_execute
    if params[:topic] == nil
      redirect_to embed_topic_path
      flash[:notice1] = "You must chose an option!"
    else
      @option = Option.find(params[:topic][:choosen_option])
      @option.vote = @option.vote + 1
      if @option.save
        flash[:notice2] = "Thanks for your vote!"
        redirect_to embed_topic_path
      else
        redirect_to embed_topic_path
      end
    end
  end
  # GET /topics
  # GET /topics.json
  def index
    @count = Topic.all.count
    if @count != 0   
    @topics = Topic.desc(:created_at).page params[:page]
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @topics }
    end
    end
  end

  # GET /topics/1
  # GET /topics/1.json
  def show
    @topic = Topic.find(params[:id])
    @users = User.all
    @comments = @topic.comments.desc(:created_at).page(params[:page]).per(2)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @topic }
    end
  end

  # GET /topics/new
  # GET /topics/new.json
  def new
    if current_user == nil
      redirect_to  user_session_path
      flash[:notice] = "You must sign in to create poll!"
    else
      @topic = Topic.new
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @topic }
      end
    end
  end

  # GET /topics/1/edit
  def edit
    @topic = Topic.find(params[:id])
  end

  # POST /topics
  # POST /topics.json
  def create1
    @topic = Topic.new(params[:topic])

    respond_to do |format|
      if @topic.save
        format.html { redirect_to @topic, notice: 'Topic was successfully created.' }
        format.json { render json: @topic, status: :created, location: @topic }
      else
        format.html { render action: "new" }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /topics
  # POST /topics.json
  def create
    if current_user == nil
      redirect_to  user_session_path
      flash[:notice] = "You must sign in to create poll!"
    else
      @user = current_user
      @topic = @user.topics.create(params[:topic])
      redirect_to topic_path(@topic)  
    end
  end

  # PUT /topics/1
  # PUT /topics/1.json
  def update
    @topic = Topic.find(params[:id])

    respond_to do |format|
      if @topic.update_attributes(params[:topic])
        format.html { redirect_to @topic, notice: 'Topic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /topics/1
  # DELETE /topics/1.json
  def destroy

    @topic = Topic.find(params[:id])
    @topic.destroy

    respond_to do |format|  
      format.html { redirect_to topics_url }
      format.json { head :no_content }
    end
  end

  def vote
    if params[:topic] == nil
      redirect_to topics_path
      flash[:notice] = "You must chose an option!"
    else
      @option = Option.find(params[:topic][:choosen_option])
      @option.vote = @option.vote + 1
      if @option.save
        flash[:success] = "Thanks for your vote!"
        redirect_to topics_path
      else
        redirect_to topics_path
      end
    end
  end
end
