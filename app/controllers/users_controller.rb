class UsersController < ApplicationController

	def index
		if current_user == nil
			redirect_to user_session_path
		else
			@user = current_user
			@topics = @user.topics.asc(:name).page params[:page]	
		end	
	end
end