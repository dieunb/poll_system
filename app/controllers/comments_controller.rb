class CommentsController < ApplicationController
  # GET /comments
  # GET /comments.json
  def index
    @comments = comment.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @comments }
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    @comment = comment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/new
  # GET /comments/new.json
  def new
    @comment = comment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/1/edit
  def edit
    @comment = comment.find(params[:id])
  end

  # POST /comments
  # POST /comments.json

  def create
    @topic = Topic.find(params[:topic_id])     
    @comment = @topic.comments.create(params[:comment])  
    if current_user == nil
      @comment.user_id = "123456" 
    else 
      @comment.user_id = current_user.id
    end
    if @comment.save
      redirect_to topic_path(@topic)
    end 
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @comment = comment.find(params[:id])

    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to @comment, notice: 'comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment = comment.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to comments_url }
      format.json { head :no_content }
    end
  end
end
